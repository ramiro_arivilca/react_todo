import React from "react";
import axios from "axios";
import "./App.css";

const BASE_URL =  "https://localhost:5001/todo/";

const Header = (props) => {
  return (
    <div className="card-header">
      <h1 className="card-header-title header">
        Tienes {props.numTodos} tareas
      </h1>
    </div>
  );
};

const TodoList = (props) => {
  const todos = props.tasks.map((todo, index) => {
    return (
      <Todo content={todo} key={index} id={index} onDelete={props.onDelete} />
    );
  });
  return <div className="list-wrapper">{todos}</div>;
};

const Todo = (props) => {
  return (
    <div className="list-item">
      {props.content.name}
      <button
        class="delete is-pulled-right"
        onClick={() => {
          props.onDelete(props.content.id);
        }}
      ></button>
    </div>
  );
};

class SubmitForm extends React.Component {
  state = { term: "" };

  handleSubmit = (e) => {
    e.preventDefault();
    if (this.state.term === "") return;
    this.props.onFormSubmit(this.state.term);
    this.setState({ term: "" });
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          type="text"
          className="input"
          placeholder="Enter Item"
          value={this.state.term}
          onChange={(e) => this.setState({ term: e.target.value })}
        />
        <button className="button">Submit</button>
      </form>
    );
  }
}

class App extends React.Component {
  state = {
    tasks: [],
  };

  componentDidMount() {
    axios
      .get(BASE_URL+"get")
      .then((res) =>
        this.setState((prevState) => ({ ...prevState, tasks: res.data }))
        
      );
  }

  handleSubmit = (task) => {
    axios.post(BASE_URL+"create", { name: task })
    .then(res => this.setState((prevState) => ({ ...prevState, tasks: res.data })))
  };

  handleDelete = (index) => {
    axios.post(BASE_URL+"delete", { id: index })
    .then(res => this.setState((prevState) => ({ ...prevState, tasks: res.data })))

  };

  render() {
    return (
      <div className="wrapper">
        <div className="card frame">
          <Header numTodos={this.state.tasks.length} />
          {this.state.tasks.length > 0 && (
            <TodoList tasks={this.state.tasks} onDelete={this.handleDelete} />
          )}
          <SubmitForm onFormSubmit={this.handleSubmit} />
        </div>
      </div>
    );
  }
}
export default App;
